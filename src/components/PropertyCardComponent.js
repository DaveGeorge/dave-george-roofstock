import React from 'react';
import { Card, CardBody, CardImg } from 'reactstrap';
import { Link } from 'react-router-dom';
import CurrencyFormat from 'react-currency-format';

import { streetAddress, cityStateZipAddress } from '../helpers/addressHelper';

const PropertyCard = ({ property }) => {
  return(
  	<Link className="property-link" to={`/properties/${property.id}`} >
      <Card className="property-card">
        <CardImg top width="100%" className="property-img" src={property.mainImageUrl} alt="Property Image" />
        <div className="property-overlay-container">
          <div className="property-img-overlay">
              <div className="property-price">{property.financial ? 
                <CurrencyFormat 
                  value={property.financial.listPrice}
                  displayType={'text'}
                  decimalScale={2}
                  fixedDecimalScale={true}
                  thousandSeparator={true}
                  prefix={'$'}
                  />
                : "Price not available"
                }
              </div>
              <div className="property-year-built">{ property.physical ? "Built in " + property.physical.yearBuilt : null}</div>
          </div>
        </div>
        <CardBody>
          <div className="row">
            <div className="col-6 property-stats">
              <h6>Monthly Rent</h6>
              <div className="property-stats-item">{property.financial ?
                <CurrencyFormat 
                  value={property.financial.monthlyRent}
                  displayType={'text'}
                  decimalScale={2}
                  fixedDecimalScale={true}
                  thousandSeparator={true}
                  prefix={'$'}
                  />
                : "N/A"}
              </div>
            </div>
            <div className="col-6 property-stats property-stats-yield">
              <h6>Gross Yield</h6>
              <div className="property-stats-item">{property.financial ?
                  (property.financial.monthlyRent * 12 / property.financial.listPrice * 100).toFixed(2) + "%"
                  : "N/A"}
              </div>
            </div>
          </div>
          <div className="row property-address">
            <div className="col-12">{streetAddress(property)}</div>
            <div className="col-12">{cityStateZipAddress(property)}</div>
          </div>
        </CardBody>
      </Card>
		</Link>
  );
} 

export default PropertyCard;