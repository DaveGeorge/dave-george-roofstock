import React, { Component } from 'react';
import { Fade } from 'react-animation-components';
import { Button, ButtonGroup, Table } from 'reactstrap';

import PropertyCard from './PropertyCardComponent';
import PropertyRow from './PropertyRowComponent';
import { Loading } from './LoadingComponent';

const CARD_VIEW = "CARD_VIEW";
const LIST_VIEW = "LIST_VIEW";

class Properties extends Component {

  constructor(props) {
    super(props);

    this.state = {
      view: CARD_VIEW // TODO: use local storage to persist the selected view
    }

    this.onViewBtnClick = this.onViewBtnClick.bind(this);
  }

  onViewBtnClick(view) {
    this.setState({ view });
  }

  render() {
    const properties = this.props.properties.properties.map((property) => {
      if(this.state.view === CARD_VIEW) {
        return (
          <div key={property.id} className="col-sm-auto mt-4">
            <PropertyCard property={property} />
          </div>
        );
      } else {
        return (
          <PropertyRow key={property.id} property={property} />
        );
      }
    });

    // TODO Theme these buttons, overriding the primary reactstrap colors using sass
    const viewSelector = (
      <ButtonGroup>
        <Button color="primary" onClick={() => this.onViewBtnClick(CARD_VIEW)} active={this.state.view === CARD_VIEW}>
          <i className="fas fa-th-large fa-lg properties-view-radio" />
        </Button>
        <Button color="primary" onClick={() => this.onViewBtnClick(LIST_VIEW)} active={this.state.view === LIST_VIEW}>
          <i className="fas fa-bars fa-lg properties-view-radio" />
        </Button>
      </ButtonGroup>
    );

    const header = (
      <div className="row m-4">
        <div className="col-6 mt-2">
          <h3>Properties</h3>
        </div>
        <div className="col-6 mt-2 properties-view-selector-container">
          {viewSelector}
        </div>
      </div>
    );

    if (this.props.properties.isLoading) {
      return (
  			<Fade in>
  	      <div className="container" >
  	        <div className="row">
  	          <Loading />
  	        </div>
  	      </div>
        </Fade>
      );
    } else if (this.props.properties.errMessage) {
      return (
        <div className="container" >
          <div className="row">
            <h4>{this.props.properties.errMessage}</h4>
          </div>
        </div>
      );
    } else if (this.state.view === CARD_VIEW) {
      return (
   	  	<Fade in>
          <div className="container">
            {header}
            <div className="row">
              {properties}
            </div>
          </div>
        </Fade>
      );
    } else {
      return (
        <Fade in>
          <div className="container">
            {header}
            <div className="container">
              <div className="row">
                <Table className="property-list" hover>
                  <thead>
                    <tr>
                      <th></th>
                      <th>Address</th>
                      <th>Price</th>
                      <th>Monthly Rent</th>
                      <th>Gross Yield</th>
                      <th>Year Built</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {properties}
                  </tbody>
                </Table>
              </div>
            </div>
          </div>
        </Fade>
      );
    }
  }
}

export default Properties;