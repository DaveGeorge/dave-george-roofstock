import React from 'react';

export const Loading = () => {
  return(
    <div className="col-12 page mt-4">
      <span className="fas fa-cog fa-spin fa-lg"></span>
      <p>Loading...</p>
    </div>
  );
}