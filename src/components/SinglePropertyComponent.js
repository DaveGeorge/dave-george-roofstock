import React, { Component } from 'react';
import { Fade } from 'react-animation-components';

import PropertyImageCarousel from './PropertyImageCarouselComponent';
import { streetAddress, cityStateZipAddress } from '../helpers/addressHelper';
import { Loading } from './LoadingComponent';

class SingleProperty extends Component {
  render() {
    if (this.props.isLoading) {
      return (
  			<Fade in>
  	      <div className="container" >
  	        <div className="row">
  	          <Loading />
  	        </div>
  	      </div>
        </Fade>
      );
    } else if (this.props.errMessage) {
      return (
        <div className="container" >
          <div className="row">
            <h4>{this.props.properties.errMessage}</h4>
          </div>
        </div>
      );
    } else {
   		return(
  			<Fade in>
  				<div className="container">
            <div className="row m-4 single-property-address">
              <div className="col-12 single-property-address1">{streetAddress(this.props.property)}</div>
              <div className="col-12 single-property-city-state-zip">{cityStateZipAddress(this.props.property)}</div>
            </div>
          </div>
          <div className="container single-property-carousel">
            <PropertyImageCarousel property={this.props.property} />
          </div>
        </Fade>
  		);
  	}
  }
}

export default SingleProperty;
