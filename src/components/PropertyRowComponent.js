import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import CurrencyFormat from 'react-currency-format';

import { streetAddress, cityStateZipAddress } from '../helpers/addressHelper';

const PropertyRow = ({ property }) => {
  const address = streetAddress(property);
  const cityStateZip = cityStateZipAddress(property);
  
  return(
    <tr>
      <th scope="row">
        <Link className="" to={`/properties/${property.id}`} >
          { 
            property.resources && property.resources.photos && property.resources.photos.length > 0
            ?
            <img className="property-icon"
                 src={property.resources.photos[0].urlSmall}
                 alt={"Investment Property at " + address + " " + cityStateZip} />
            :
            null
          }
        </Link>
      </th>
      <td>
        <Link className="property-row-link" to={`/properties/${property.id}`} >
          <div className="property-row-address1">{address}</div>
          <div className="property-row-city-state-zip">{cityStateZip}</div>
        </Link>
      </td>
      <td>
        {
          property.financial
          ? 
          <CurrencyFormat 
            value={property.financial.listPrice}
            displayType={'text'}
            decimalScale={2}
            fixedDecimalScale={true}
            thousandSeparator={true}
            prefix={'$'}
            />
          :
          "N/A"
        }
      </td>
      <td>
        {
          property.financial
          ?
          <CurrencyFormat 
            value={property.financial.monthlyRent}
            displayType={'text'}
            decimalScale={2}
            fixedDecimalScale={true}
            thousandSeparator={true}
            prefix={'$'}
            />
          :
          "N/A"
        }
      </td>
      <td>
        {
          property.financial
          ?
          (property.financial.monthlyRent * 12 / property.financial.listPrice * 100).toFixed(2) + "%"
          :
          "N/A"
        }
      </td>
      <td>
        { property.physical ? property.physical.yearBuilt : "N/A" }
      </td>
      <td>
        {/*TODO, make button color part of a custom reactstrap theme*/}
        <Link to={`/properties/${property.id}`} >
          <Button color="primary">See Details</Button>
        </Link>
      </td>
    </tr>
  );
} 

export default PropertyRow;