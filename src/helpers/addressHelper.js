import React from 'react';

export const streetAddress = (property) => {
	return property.address ? property.address.address1 : "N/A";
}

export const cityStateZipAddress = (property) => {
	return property.address ? property.address.city 
                        + ", " 
                        + property.address.state 
                        + " " 
                        + property.address.zip : "N/A";
}