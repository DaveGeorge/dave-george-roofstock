import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import './App.css';
import { ConfigureStore } from './store/configureStore';
import Main from './containers/MainContainer';

const store = ConfigureStore();

class App extends Component {
  render() {
    return (
    	<Provider store={store}>
	      <BrowserRouter>
	        <div className="App">
	          <Main />
	        </div>
	      </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
