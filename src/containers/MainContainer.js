import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { Fade } from 'react-animation-components';
import { connect } from 'react-redux';

import Properties from '../components/PropertiesComponent';
import SingleProperty from '../components/SinglePropertyComponent';
import { fetchProperties } from '../store/ActionCreators';

class Main extends Component {
  componentDidMount() {
    this.props.fetchProperties();
  }
  
  render() {
    const PropertyWithId = ({match}) => {
      return(
        <SingleProperty
          property={this.props.properties.properties.filter((property) => property.id === parseInt(match.params.id,10))[0]} 
          isLoading={this.props.properties.isLoading}
          errMess={this.props.properties.errMess}
        />
      );
    }

    return(
      <div>
        <Fade in>
          <Switch>
            <Route exact path="/properties" component={() => <Properties properties={this.props.properties} />} />
            <Route path="/properties/:id" component={PropertyWithId} />
            <Redirect to="/properties" />
          </Switch>
        </Fade>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    properties: state.properties
  }  
}

const mapDispatchToProps = (dispatch) => ({
  fetchProperties: () => {dispatch(fetchProperties())}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));