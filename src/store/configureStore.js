import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Properties } from './properties';

export const ConfigureStore = () => {
  const store = createStore(
    combineReducers({
      properties: Properties
    }),
    applyMiddleware(thunk)
  );

  return store;
}