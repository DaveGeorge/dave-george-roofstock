import * as ActionTypes from './ActionTypes';

export const fetchProperties = () => (dispatch) => {
  dispatch(propertiesLoading(true));

  return fetch('http://dev1-sample.azurewebsites.net/properties.json')
    .then(response => {
      if (response.ok) {
        return response;
      } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
      }
    },
    error => {
      var errmess = new Error(error.message);
      throw errmess;
    })
    .then(response => response.json())
    .then(properties => dispatch(addProperties(properties.properties)))
    .catch(error => dispatch(propertiesFailed(error.message)));
}

export const addProperties = (properties) => ({
  type: ActionTypes.ADD_PROPERTIES,
  payload: properties
});

export const propertiesLoading = () => ({
  type: ActionTypes.PROPERTIES_LOADING
});

export const propertiesFailed = (errmess) => ({
  type: ActionTypes.PROPERTIES_FAILED,
  payload: errmess
});
