import * as ActionTypes from './ActionTypes';

export const Properties = (state = {
  isLoading: true,
  errMessage: null,
  properties: []
}, action) => {
  switch(action.type) {
    case ActionTypes.ADD_PROPERTIES:
      return {...state, isLoading: false, errMessage: null, properties: action.payload}
    case ActionTypes.PROPERTIES_LOADING:
      return {...state, isLoading: true, errMessage: null, properties: []}
    case ActionTypes.PROPERTIES_FAILED:
      return {...state, isLoading: false, errMessage: action.payload, properties: []}
    default: 
      return state;
  }
}