This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn`

Installs all the node modules needed to run the app.  This may take awhile.

Note: If you haven't got yarn installed on your system, you can get it at [https://yarnpkg.com/lang/en/docs/install](https://yarnpkg.com/lang/en/docs/install).

### `yarn start`

This will start the node server and will try to launch the app in your browser, usually at [http://localhost:3000/](http://localhost:3000/).
